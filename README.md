# Dcoker LDAPTOOLBOX - Self Service Password

Le projet `ldaptoolbox - Self Service Password` permet à chaque utilisateur de changer lui même son mot de passe.

https://ltb-project.org/documentation/self-service-password


Ce docker utilise la version 1.3 du projet.

Dans la documentation, il est proposé une liste de variables de configuration par défaut pouvant être surchargée. Celles-ci sont définies dans le ficheir `config.inc.local.php`. Pour les renseigner dans votre `docker run` ou `docker-compose.yml`, il faut précéder ces noms de variables par `LTB_`.

Par exemple pour passer `ldaptoolbox` en mode `debug`, on voudra définir `$debug=true`. Pour se faire, on renseigne la variable environnent `LTB_debug=true` dans votre `docker`.


| Variable | Valeur | Usage |
| ---- | ---- | ---- |
| SITE_HOST_NAME | URL d'accès au service | exemple : ldaptoolbox.domaine.fr Si cette variable n'est pas renseigné, le certificat pour HTTPS sera créé avec le fqdn du container |
| LTB_nom-de-la-variable-a-surcharger | voir la documentation de ldaptoolbox | Si vous souhaitez définir une variable proposée par ldaptoolbox, inscrivez là, en respectant les majuscules/minuscules, précédez de LTB_ |


Le projet docker ldaptoolbox - self service password est hébergé sur le Gitlab ADULLACT :
https://gitlab.adullact.net/libricielscop/ldaptoolbox-ssp
