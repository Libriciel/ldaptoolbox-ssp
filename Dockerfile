FROM php:7.3-apache-buster

ENV LTBVERSION=1.3

#Installation des composants
RUN apt-get update && \
    apt-get dist-upgrade -y && \
    apt-get install -y \
    libldap2-dev \
    wget \
    pwgen \
    msmtp \
    msmtp-mta \
    mailutils vim

RUN docker-php-ext-install mbstring \
  ldap

# Copie des scripts
COPY ./scripts/*.sh /usr/local/bin/
RUN chmod 555 /usr/local/bin/*.sh
COPY ./config/v3.ext /opt/

# Mise en place ldaptoolbox
RUN cd /tmp && \
  wget http://ltb-project.org/archives/ltb-project-self-service-password-${LTBVERSION}.tar.gz && \
  tar -xvzf ltb-project-self-service-password-${LTBVERSION}.tar.gz &&\
  mv ltb-project-self-service-password-${LTBVERSION} /var/www/ltb-project-self-service-password &&\
  chown -R www-data:www-data /var/www/ltb-project-self-service-password
RUN echo "TLS_REQCERT allow" >> /etc/ldap/ldap.conf

# Apache
COPY ./config/ldaptoolbox_vhost_ssl.conf /etc/apache2/sites-available/
RUN a2dissite 000-default &&\
  a2ensite ldaptoolbox_vhost_ssl.conf &&\
  a2enmod rewrite &&\
  a2enmod ssl &&\
  a2enmod headers


 EXPOSE 443
 ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
 CMD ["apache2-foreground"]
