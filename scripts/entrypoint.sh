#!/bin/bash

if [ -z ${SITE_HOST_NAME} ]
then
  SITE_HOST_NAME=`hostname -f`
fi

if [ -z ${LTB_keyphrase} ]
then
  export LTB_keyphrase=`pwgen -n 20 1`
fi

echo "Generation des certificats SSL"
/usr/local/bin/generate-key-pair.sh ${SITE_HOST_NAME}

echo "Génération du fichier de configuration LTB"
/usr/local/bin/ltb-generate-config.sh

#source /etc/apache2/envvars && exec /usr/sbin/apache2 -DFOREGROUND
exec "$@"
