#!/bin/bash

FILECONFIG="/var/www/ltb-project-self-service-password/conf/config.inc.local.php"
echo "<?php" > ${FILECONFIG}
echo "" >> ${FILECONFIG}


for varltb in `env | grep -e "^LTB_" | cut -d'=' -f1`
do
  echo "Variable commençant par LTB_ : ${varltb}"
  conf=`echo ${varltb} | sed -e"s/^LTB_//g"`
  if [ "${!varltb}" == true ] || [ "${!varltb}" == false ]
  then
    echo '$'${conf}' = '${!varltb}';' >> ${FILECONFIG}
  else
      echo '$'${conf}' = "'${!varltb}'";' >> ${FILECONFIG}
  fi
  echo "" >> ${FILECONFIG}
done
